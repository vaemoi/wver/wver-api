const bsConfig = {
  open: false,
  files: [`${__dirname}/scripts/auth.js`],
  plugins: [{
    module: `bs-html-injector`,
    options: {
      files: [`${__dirname}/public/index.html`]
    }
  }],
  server: `${__dirname}/public`
};

require(`browser-sync`).init(bsConfig);
