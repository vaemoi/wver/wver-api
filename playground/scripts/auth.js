document.addEventListener(`DOMContentLoaded`, function() {
  try {
    if (!window.Auth0Lock) {
      throw new Error(`Auth0 not available`);
    }

    let audience = null, clientID = null;

    if (process.env.NODE_ENV === `production`) {
      clientID = process.env.AUTH0_CLIENT_ID_PROD;
      audience = process.env.AUTH0_AUDIENCE_PROD;
    } else {
      clientID = process.env.AUTH0_CLIENT_ID_STAGE;
      audience = process.env.AUTH0_AUDIENCE_STAGE;
    }

    const lock = new window.Auth0Lock(clientID, process.env.AUTH0_DOMAIN, {
      configurationBaseUrl: `https://cdn.auth0.com`,
      auth: {
        audience: audience,
        responseType: `token`,
        params: {
          scope: `openid`,
          sso: false
        }
      }
    });

    lock.on(`authenticated`, (authResult) => {
      window.location.hash = ``;

      if (!authResult) {
        throw new Error(`Authentication failed`);
      }

      lock.hide();
      const result = document.getElementById(`result`);
      const token = document.getElementById(`token`);

      result.style.visibility = `visible`;
      token.textContent = `Access Token: ${authResult.accessToken}`;
    });

    const authenticateButton = document.getElementById(`authenticate`);

    authenticateButton.addEventListener(`click`, () => lock.show());

  } catch (err) {
    console.log(`Error: ${err.message}`);
  }
});
