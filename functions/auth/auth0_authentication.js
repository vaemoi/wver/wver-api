const jwt = require(`jsonwebtoken`);
const jwksRsa = require(`jwks-rsa`);
const fromEvent = require(`graphcool-lib`).fromEvent;

const
  verifyToken = (token, domain, audience) => new Promise((resolve, reject) => {
    const { header } = jwt.decode(token, { complete: true });

    if (!header || !header.kid) {
      reject(new Error(`Invalid token`));
    }

    if (header.alg !== `RS256`) {
      reject(new Error(`Wrong signature, expected RS256 got: ${header.alg}`));
    }

    const jwksClient = jwksRsa({
      cache: true,
      jwksUri: `https://${domain}/.well-known/jwks.json`
    });

    jwksClient.getSigningKey(header.kid, (keyError, key) => {
      if (keyError) {
        reject(new Error(`Error getting signing key: ${keyError.message}`));
      }

      const signingKey = key.publicKey || key.rsaPublicKey;

      jwt.verify(
        token,
        signingKey,
        {
          algorithms: [`RS256`],
          audience: audience,
          ignoreExpiration: false,
          issuer: `https://${domain}/`
        },
        (verifyError, decoded) => {
          if (verifyError) {
            reject(new Error(`${verifyError.message}`));
          }

          resolve(decoded);
        }
      );
    })
  }),

  fetchRevUser = (auth0UserId, api) => api.request(
    `
      query getUser($auth0UserId: String!) {
        User(auth0UserId: $auth0UserId) {
          id
        }
      }
    `, { auth0UserId }
  ).then((queryResult) => queryResult.User),

  createRevUser = (auth0UserId, api) => api.request(
    `
      mutation createUser($auth0UserId: String!) {
        createUser(auth0UserId: $auth0UserId) {
          id
        }
      }
    `, { auth0UserId }
  ).then((queryResult) => queryResult.createUser);


export default async (evt) => {
  try {
    if (!process.env.AUTH0_DOMAIN || !process.env.AUTH0_AUDIENCE) {
      throw new Error(`Missing required env variables`);
    }

    const { accessToken } = evt.data;

    // Decode the token
    const decodedToken = await verifyToken(
      accessToken, process.env.AUTH0_DOMAIN, process.env.AUTH0_AUDIENCE
    );

    const
      GRAPHCOOL = fromEvent(evt),
      api = GRAPHCOOL.api(`simple/v1`);

    // Lookup user
    let user = await fetchRevUser(decodedToken.sub, api);

    // Create user if lookup was unsuccessful
    if (!user) {
      user = await createRevUser(decodedToken.sub, api);
    }

    if (!user) {
      throw new Error(`Problem creating user`);
    }

    // Generate an auth token for next query
    const token = await GRAPHCOOL.generateNodeToken(
      user.id,
      `User`,
      decodedToken.exp
    )

    return { data: { id: user.id, token: token } };
  } catch (error) {
    return { error: {
      debugMessage: `${error.message}`,
      userMessage: `Ahh poop, something fudged on our end!`
    }};
  }
};
