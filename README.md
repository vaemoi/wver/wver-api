# wver-api
wver's API powered by GraphCool & Auth0
---

### Folder Breakdown
* functions => Helper functions, custom mutations, schema extensions for graphcool api
* playground => Simple devserver to get authentication token for accessing the graphcool playground
* rules => Functions for performing operations durig login/logout/signup



Clients:
 - [wver](https://wver.xyz): Web app for reviewing code
     + Language -> javascript
 - [wver-cli-ruby](https://gitlab.com/vaemoi/wver/wver-cli-ruby): Cli tool for creating revs
     + Language -> ruby
